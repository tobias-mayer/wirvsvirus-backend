FROM openjdk:11-jdk-slim

ADD ./target/backend-1.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
EXPOSE 8080

